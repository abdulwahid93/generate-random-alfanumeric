package com.engine.aw;

import java.util.Random;
import java.util.Scanner;
import java.util.logging.Logger;
import java.util.logging.Level;

public class GenerateRandomAlfanumeric {

	private static final Logger logger = Logger.getLogger(GenerateRandomAlfanumeric.class.getSimpleName());
	private static Random random = new Random();
	

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		logger.log(Level.INFO, "Input How Much to be Generate Code Random: ");
		Scanner scannerCountCodeRandom = new Scanner(System.in);
		int countCodeRandom = scannerCountCodeRandom.nextInt();
		for (int countGenerate = 1; countGenerate <= countCodeRandom; countGenerate++) {
			String code = generateRandomAlfanumeric();
			logger.log(Level.INFO, "Code {0}", countGenerate + ": " + code);
		}

	}

	private static String generateRandomAlfanumeric() {
		char[] chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".toCharArray();
		StringBuilder sb = new StringBuilder(10);
		for (int i = 0; i < 10; i++) {
			char c = chars[random.nextInt(chars.length)];
			sb.append(c);
		}
		return sb.toString();
	}
}
